import pymongo
import json
from datetime import date, timedelta, datetime

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter_politics2"]
politics_col = mydb["politics"]
partys_col = mydb["partys"]
politics_followers_col = mydb["politics_followers"]


def get_chart_data(entity, days_ago):
    result = []
    today = datetime.today()
    x_days_before = today - timedelta(days=days_ago)
    x_days_before = x_days_before.strftime("%Y-%m-%d")
    if entity=="person": 
        with open('modules/twitter/person_names.json') as json_file:  
            persons_names = json.load(json_file)
            for person_list in persons_names:
                total_appearances = politics_col.find({ 'search_keyword': { '$in': person_list }, 'date': { '$gte': x_days_before } }).count()
                result.append({'Name': person_list, 'Count': total_appearances})
    if entity=="party": 
        with open('modules/twitter/partys_names.json') as json_file:  
            partys_names = json.load(json_file)
            for partys_list in partys_names:
                total_appearances = partys_col.find({ 'search_keyword': { '$in': partys_list }, 'date': { '$gte': x_days_before } }).count()
                result.append({'Name': partys_list, 'Count': total_appearances})
    json_file.close()
    myclient.close()
    return result
    

def get_followers_chart_data():
    result = []
    followers_count = politics_followers_col.find({},{'_id': 0})
    for obj in followers_count:
        result.append({'Name': obj['name'], 'Count': obj['followers_count']})
    myclient.close()
    return result


def get_retweet_chart_data(entity,days_ago):
    result = []
    today = datetime.today()
    x_days_before = today - timedelta(days=days_ago)
    x_days_before = x_days_before.strftime("%Y-%m-%d")
    if entity=="person":
        with open('modules/twitter/person_names.json') as json_file:
            persons_names = json.load(json_file)
            for person_list in persons_names:
                # total_followers = politics_col.find({ 'search_keyword': { '$in': person_list } },{ 'total_followers': {'$sum': '$followers_count'} }, {}).
                total_followers = politics_col.aggregate([
                    {
                        "$match": {
                            "search_keyword": { '$in': person_list },
                            'date': { '$gte': x_days_before }
                        }
                    },
                    {
                        "$group": {
                            "_id": 1,
                            "total_followers": { "$sum": { "$sum": "$retweet_count"} }
                        }
                    }
                ])
                try:
                    count = total_followers.next()["total_followers"]
                    result.append({'Name': person_list, 'Count': count})
                except StopIteration:
                    print("")
    if entity=="party": 
        with open('modules/twitter/partys_names.json') as json_file:  
            partys_names = json.load(json_file)
            for partys_list in partys_names:
                total_followers = partys_col.aggregate([
                    {
                        "$match": {
                            "search_keyword": { "$in": partys_list },
                            "date": { "$gte": x_days_before }
                        }
                    },
                    {
                        "$group": {
                            "_id": 1,
                            "total_followers": { "$sum": { "$sum": "$retweet_count"} }
                        }
                    }
                ])
                try:
                    count = total_followers.next()["total_followers"]
                    result.append({'Name': partys_list, 'Count': count})
                except StopIteration:
                    print("")
    json_file.close()
    myclient.close()
    return result
