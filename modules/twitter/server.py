from flask import Flask, render_template, send_from_directory, request, jsonify, Blueprint
from .get_chart_data import *

# FLASK_APP=server.py flask run

twitter_blueprint = Blueprint('twitter_page', __name__,
                        template_folder='templates',
                        static_folder='static',
                        static_url_path='/modules/twitter/server/static')

twitter_blueprint.secret_key = b'_5#y2L"F4Q8z111t\n\xec]/'

@twitter_blueprint.route('/twitter_bar_chart')
def update_bar_chart():
    return render_template('twitter_bar_chart.html', server_url="http://127.0.0.1:5000/")

@twitter_blueprint.route("/occurrences", methods=['POST'])
def chart_data():
    entity = request.get_json()['entity']
    days_ago = request.get_json()['days_ago']
    chart_data = get_chart_data(entity, days_ago)
    return jsonify(chart_data)

@twitter_blueprint.route("/followers", methods=['POST'])
def followers_chart_data():
    chart_data = get_followers_chart_data()
    return jsonify(chart_data)

@twitter_blueprint.route("/retweets", methods=['POST'])
def retweet_chart_data():
    entity = request.get_json()['entity']
    days_ago = request.get_json()['days_ago']
    chart_data = get_retweet_chart_data(entity,days_ago)
    return jsonify(chart_data)

