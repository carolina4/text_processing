import pymongo
from twython import Twython  
import json
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
#from apscheduler.scheduler import Scheduler
from apscheduler.schedulers.background import BackgroundScheduler
import tweepy
import time

def save_biggest_id():
    # Get biggest twiter id
    last_ids_col.delete_many({})
    # politics
    biggest_id = politics_col.find().sort("tweet_id",-1).limit(1)
    last_ids_col.insert_one({'biggest_id': biggest_id.next()["tweet_id"], 'type': 'politics'})
    # partys
    biggest_id = partys_col.find().sort("tweet_id",-1).limit(1)
    last_ids_col.insert_one({'biggest_id': biggest_id.next()["tweet_id"], 'type': 'partys'})


def make_person_query():
    with open('modules/twitter/person_names.json') as json_file:  
        persons_names = json.load(json_file)
        max_politics_id = last_ids_col.find({'type': 'politics'}).next()['biggest_id']
        for person_list in persons_names:
            for person in person_list:
                query = {'q': person,  
                    'count': 100,
                    'since_id': max_politics_id
                }
                # Search tweets  
                for status in python_tweets.search(**query)['statuses']:  

                    tweet_dict = {'search_keyword': person_list, 
                        'coordinates':[], 
                        'username': [], 
                        'followers_count': [], 
                        'retweet_count': [], 
                        'text': [], 
                        'created_at': [],
                        'hashtags': [],
                        'lang': [],
                        'tweet_id': '',
                        'date': ''} 
                    tweet_dict['coordinates'].append(status['coordinates'])
                    tweet_dict['username'].append(status['user']['name']) 
                    tweet_dict['followers_count'].append(status['user']['followers_count']) 
                    tweet_dict['retweet_count'].append(status['retweet_count']) 
                    tweet_dict['text'].append(status['text'])
                    tweet_dict['created_at'].append(status['created_at']) 
                    tweet_dict['hashtags'].append(status['entities']['hashtags'])
                    tweet_dict['lang'].append(status['lang'])
                    tweet_dict['tweet_id'] = status['id']
                    tweet_dict['date'] = datetime.today().strftime('%Y-%m-%d')
                    politics_col.insert_one(tweet_dict)
    json_file.close()


def make_party_query():
    # Query Twitter partys ##################################################
    with open('modules/twitter/partys_names.json') as json_file:  
        partys_names = json.load(json_file)
        max_partys_id = last_ids_col.find({'type': 'partys'}).next()['biggest_id']
        for partys_list in partys_names:
            for party in partys_list:
                query = {'q': party,  
                    'count': 100,
                    'since_id': max_partys_id
                }
                # Search tweets  
                for status in python_tweets.search(**query)['statuses']:  

                    tweet_dict = {'search_keyword': partys_list, 
                        'coordinates':[], 
                        'username': [], 
                        'followers_count': [], 
                        'retweet_count': [], 
                        'text': [], 
                        'created_at': [],
                        'hashtags': [],
                        'lang': [],
                        'tweet_id': '',
                        'date': ''} 
                    tweet_dict['coordinates'].append(status['coordinates'])
                    tweet_dict['username'].append(status['user']['name']) 
                    tweet_dict['followers_count'].append(status['user']['followers_count']) 
                    tweet_dict['retweet_count'].append(status['retweet_count']) 
                    tweet_dict['text'].append(status['text'])
                    tweet_dict['created_at'].append(status['created_at']) 
                    tweet_dict['hashtags'].append(status['entities']['hashtags'])
                    tweet_dict['lang'].append(status['lang'])
                    tweet_dict['tweet_id'] = status['id']
                    tweet_dict['date'] = datetime.today().strftime('%Y-%m-%d')

                    partys_col.insert_one(tweet_dict)
    json_file.close()


def update_persons_followers():
    auth = tweepy.OAuthHandler(creds['CONSUMER_KEY'], creds['CONSUMER_SECRET'])
    auth.set_access_token(creds['ACCESS_TOKEN'], creds['ACCESS_SECRET'])
    api = tweepy.API(auth, wait_on_rate_limit=True)

    politics_followers_col.delete_many({})
    #associate user with twitter name
    with open('modules/twitter/politics_twitter_names.json') as json_file:  
        politics_twitter_names = json.load(json_file)

        for idx, target in enumerate(politics_twitter_names):
            try:
                user = api.get_user(target[1])
                politics_followers_col.insert_one({'name': target[0], 'followers_count': user.followers_count})
                # outfile.writerow({'uID': target, 'Username': user.name, 'Follower Count': user.followers_count, 'Verified': user.verified})
            except tweepy.TweepError as e:
                # outfile.writerow(e.message)
                print(e.message)
    json_file.close()


def clean_old_tweets():
    today = datetime.today()
    yesterday = today - timedelta(days=1)
    lastMonth = yesterday + relativedelta(months=-1)
    lastMonth = lastMonth.strftime("%Y-%m-%d")
    partys_col.delete_many({ 'date': { '$lte': lastMonth } })
    politics_col.delete_many({ 'date': { '$lte': lastMonth } })


def update_database():
    clean_old_tweets()
    make_party_query()
    make_person_query()
    save_biggest_id()
    update_persons_followers()

    myclient.close()

# Twitter ###############################################################
# Load credentials from json file
with open("modules/twitter/twitter_credentials.json", "r") as file:  
    creds = json.load(file)

# Instantiate an object
python_tweets = Twython(creds['CONSUMER_KEY'], creds['CONSUMER_SECRET'])


# Mongo DB ##############################################################
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["twitter_politics2"]
politics_col = mydb["politics"]
partys_col = mydb["partys"]
last_ids_col = mydb["last_ids"]
politics_followers_col = mydb["politics_followers"]

# Scheduller ############################################################
# Start the scheduler
sched = BackgroundScheduler()
sched.add_job(update_database, 'cron', day_of_week = '*', hour = '10', minute = '43', second = '0')
sched.start()

while True:
    time.sleep( 1 )

'''
politics_col.delete_many({})
partys_col.delete_many({})
'''
