from flask import Flask, flash, request, redirect, url_for, render_template, send_from_directory, jsonify, json
import os

from text_processing import app

from ..modules.text2data.server.web_server import text2data_blueprint
from ..modules.politics_chart.backend import politicsChart_blueprint
from ..modules.twitter.server import twitter_blueprint

from flask import Flask
from flask_cors import CORS

app = Flask(__name__, static_folder='static', static_url_path='/server/static')
CORS(app)

# register blueprints
app.register_blueprint(text2data_blueprint)
app.register_blueprint(politicsChart_blueprint)
app.register_blueprint(twitter_blueprint)

# FLASK_APP=server/web_server.py flask run

# server url
SERVER_URL = "http://127.0.0.1:5000/"

@app.route('/')#, methods=['POST']
def render_main_page():
    return render_template('home.html', text2data_url=SERVER_URL+"pdf2data", politicsChart_url=SERVER_URL+"bar_chart", twitterChart_url=SERVER_URL+"twitter_bar_chart")



