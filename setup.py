from setuptools import setup

setup(
    name='text_processing',
    packages=['server','modules/text2data'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)